import asyncio
import json
import ssl
import time

from aiohttp import web
import socketio

with open('config.json') as config_file:
    conf = json.load(config_file)

sio = socketio.AsyncServer(async_mode='aiohttp')
app = web.Application()
sio.attach(app)
ssl_context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
ssl_context.load_cert_chain('cert.pem', 'key.pem')


async def emit_event_task():
    """sends current time within a string every five seconds to a room specified in config.json"""
    while True:
        await sio.sleep(5)
        await sio.emit('room_event', {'data': f'Server message {time.time()}'}, to=conf['room_name'])


@sio.event
async def join(sid, message):
    sio.enter_room(sid, message['room'])
    await sio.emit('room_event', {'data': 'Entered room: ' + message['room']},
                   room=sid)


@sio.event
async def leave(sid, message):
    sio.leave_room(sid, message['room'])
    await sio.emit('message', {'data': 'Left room: ' + message['room']},
                   room=sid)


@sio.event
async def system_status_event(sid, message):
    print('session ', sid, 'message ', message)


@sio.event
async def connect(sid, environ):
    await sio.emit('message', {'data': 'Connected'}, room=sid)


@sio.event
def disconnect(sid):
    print('Client disconnected')


if __name__ == '__main__':
    sio.start_background_task(emit_event_task)
    web.run_app(app, ssl_context=ssl_context)
