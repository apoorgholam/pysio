import asyncio

from quart import Quart , Response

from socket_io_clinet import start_client

app = Quart(__name__)

@app.route('/')
async def hello():
    return Response("<a href='/socket_io_client/'>to get the socket_io client started click here</a>")

@app.route('/socket_io_client/', methods=['GET'])
async def create_job():
    asyncio.ensure_future(start_client())
    return 'Successfully started the client see the std out of server process for more info'

app.run(certfile='cert.pem',keyfile='key.pem')