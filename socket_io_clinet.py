import json
import asyncio
import urllib.request

import psutil
import socketio

external_ip = urllib.request.urlopen('https://ident.me').read().decode('utf8')

loop = asyncio.get_event_loop()
sio = socketio.AsyncClient()

with open('config.json') as config_file:
    conf = json.load(config_file)


async def send_system_staus():
    system_status = {
        'IP': external_ip,
        'CPU': psutil.cpu_percent(),
        'RAM': psutil.virtual_memory()[2],
    }
    print('sending system status...')
    await sio.emit('system_status_event', {'room': external_ip, 'data': system_status},)


@sio.event
async def room_event(data):
    print('data from joined room', data)


@sio.event
async def connect():
    print('connected to server')
    await sio.emit('join', {'room': conf['room_name']})
    while True:
        await send_system_staus()
        await sio.sleep(5)


async def start_client():
    await sio.connect('https://0.0.0.0:8443')
    await sio.wait()


if __name__ == '__main__':
    loop.run_until_complete(start_client())
